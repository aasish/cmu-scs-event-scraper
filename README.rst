========================
CMU SCS Calendar Scraper
========================
This program scrapes cmu scs events calendar and prints to stdout.

Calendar format as of 2014-02-08                                     
http://www.cs.cmu.edu/calendar-month?month=2014-02                   

Usage
======
 
 python scs_events.py -b 2014-01 -e 2014-04
 
Output
======
The program prints a list of attributes of the events in a tsv format.

- building
- room    
- title  
- url    
- text    
- speaker 
- date    
- event-type    
- description

Dependencies
============

- Python 2.7+

- BeautifulSoup 

  http://www.crummy.com/software/BeautifulSoup/ 
    
