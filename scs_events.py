#!/usr/bin/env python

###########################################################################
##                                                                       ##
##                  Language Technologies Institute                      ##
##                     Carnegie Mellon University                        ##
##                         Copyright (c) 2014                            ##
##                        All Rights Reserved.                           ##
##                                                                       ##
##  Permission is hereby granted, free of charge, to use and distribute  ##
##  this software and its documentation without restriction, including   ##
##  without limitation the rights to use, copy, modify, merge, publish,  ##
##  distribute, sublicense, and/or sell copies of this work, and to      ##
##  permit persons to whom this work is furnished to do so, subject to   ##
##  the following conditions:                                            ##
##   1. The code must retain the above copyright notice, this list of    ##
##      conditions and the following disclaimer.                         ##
##   2. Any modifications must be clearly marked as such.                ##
##   3. Original authors' names are not deleted.                         ##
##   4. The authors' names are not used to endorse or promote products   ##
##      derived from this software without specific prior written        ##
##      permission.                                                      ##
##                                                                       ##
##  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         ##
##  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ##
##  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ##
##  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      ##
##  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ##
##  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ##
##  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ##
##  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ##
##  THIS SOFTWARE.                                                       ##
##                                                                       ##
###########################################################################
##                                                                       ##
##  Author: Aasish Pappu (aasish@cs.cmu.edu)                             ##
##  Date  : February 2014                                                ##
###########################################################################
##  This program scrapes cmu scs events calendar and prints to  stdout   ##                                 
##  Calendar format as of 2014-02-08                                     ##
##  http://www.cs.cmu.edu/calendar-month?month=2014-02                   ##
###########################################################################

try:
    from bs4 import BeautifulSoup

except ImportError:
    try: 
        from BeautifulSoup import BeautifulSoup
    except:
        print >> sys.stderr, ">> beautifulsoup required"
        sys.exit(1)


import argparse
from cookielib import CookieJar
import re
import sys

from urllib2 import Request, build_opener, HTTPCookieProcessor
from urllib import quote




encode = lambda s: s.encode('utf-8')

DEBUG = False 
class EventParser:
    def __init__(self):
        self.soup = None
        self.event_template = {'type':None, 
                                'speaker':None,
                                'title': None,
                                'dept': None,
                                'date': None,
                                'room': None,
                                'building': None,
                                'description': None
                            }
    def parse(self, event, output):
        
        for k in self.event_template:
            if k in event:
                pass
            else:
                event[k] = str(self.event_template[k])

        self.soup = BeautifulSoup(output)
        
        try:
            event['type'] = self.soup.find('div', class_='views-field views-field-field-event-type').find(class_='field-content').get_text()
        except:
            if DEBUG:
                print >> sys.stderr, '>> cannot find the type for the event %s' %event['url']
            
            pass
                
        try:
            dept = self.soup.find('div', class_='views-field views-field-field-speaker-title').find(class_='field-content').get_text()
            event['dept'] = dept
        except:
            if DEBUG:
                print >> sys.stderr, '>> cannot find the speaker title for the event %s' %event['url']
            
            pass 

        try:
            event['speaker'] = self.soup.find('div', class_='views-field views-field-field-speaker-').find(class_='field-content').get_text()
        except:
            if DEBUG:
                print >> sys.stderr, '>> cannot find the speaker for the event %s' %event['url']
            
            pass 
            
        try:
            event['title'] = self.soup.find('div', class_='views-field views-field-field-talk-title').find(class_='field-content').get_text()
        except:
            if DEBUG:
                print >> sys.stderr, '>> cannot find the title for the event %s' %event['url']
                
            pass
        try:
            event['date'] = self.soup.find('div', class_='views-field views-field-field-event-date2').find(class_='field-content').get_text()
        except:
            if DEBUG:
                print >> sys.stderr, '>> cannot find the date for the event %s' %event['url']
            pass
                
        try:
            event['room'] = self.soup.find('div', class_='views-field views-field-field-room-number').find(class_='field-content').get_text()
        except:
            if DEBUG:
                print >> sys.stderr, '>> cannot find the room for the event %s' %event['url']
            pass
            
        try:
            event['building'] = self.soup.find('div', class_='views-field views-field-field-building-and-room-number').find(class_='field-content').get_text()
        except:
            if DEBUG:
                print >> sys.stderr, '>> cannot find the building for the event %s' %event['url']
            pass
              
        try:
            body = self.soup.find('div', class_='views-field views-field-body').find(class_='field-content').get_text()
            event['description'] = body.replace('<p>', '').replace('\n','')
        except:
            if DEBUG:
                print >> sys.stderr, '>> cannot find the body for the event %s' %event['url']
            pass
            
            
            return event
       
            

class CalendarParser:
    def __init__(self):
        self.soup = None

    def parse(self, output):
        
            self.soup = BeautifulSoup(output)
            events = self.soup.find_all('div', class_='view-item view-item-scs_calendar')
            events_info = []
            for event in events:
                event_info = {}
                event_content = event.find('div', class_='contents')
                event_link = event_content.find('a')
                
                event_info['url'] = event_link.get('href')
                event_info['text'] = event_link.get_text()
                
                try:
                    event_room = event_content.find(class_='views-field views-field-field-room-number')
                    event_info['room'] = event_room.find(class_='field-content').get_text()
                except:
                    if DEBUG:
                        print >> sys.stderr, '>> cannot find room for event %s' %(event_info['url'])
                    pass

                try:
                    event_building = event_content.find(class_='room')
                    event_info['building'] = event_building.get_text()
                except:
                    if DEBUG:
                        print >> sys.stderr, '>> cannot find building for event %s' %(event_info['url'])
                    pass
                
                events_info.append(event_info)
            
            return events_info
            

class EventQuery:
    
    CAL_URL = 'http://www.cs.cmu.edu/calendar-month?month=%(month)s'
    SCS_URL = 'http://www.cs.cmu.edu%(event)s'
    USER_AGENT = 'Mozilla/5.0 (X11; U; FreeBSD i386; en-US; rv:1.9.2.9) Gecko/20100913 Firefox/3.6.9'
    def __init__(self, begin, end):
        self.begin = begin
        self.end = end
        

        self.cjar = CookieJar()
        self.opener = build_opener(HTTPCookieProcessor(self.cjar))
        self.cal_parser = CalendarParser()
        self.event_parser = EventParser() 

    def _get_events_per_month(self, month):
        url = self.CAL_URL % {'month':quote(encode(month))}
        output = self._web_request(url)
        events = self.cal_parser.parse(output)
        len_events = len(events)
        for i in range(len_events):
            event = events[i]
            event = self._get_event(event)
            if event:
                events[i] = event
        return events

    def _get_event(self, event):
        event_url = event['url']
        url = self.SCS_URL % {'event':event_url}
        output = self._web_request(url)
        event_tmp = self.event_parser.parse(event, output)
        if event_tmp:
            event = event_tmp
        return event

    def _web_request(self, url):
        request = Request(url=url, headers={'User-Agent':self.USER_AGENT})
        headers = self.opener.open(request)
        output = headers.read()
        return output
        
    def getEvents(self):
        month = self.begin
        year, mon = month.split('-')
        year = int(year)
        mon = int(mon)
        
        events = []
        while month != self.end:
            mon_events = self._get_events_per_month(month)
            events.extend(mon_events)
            if mon < 12:
                mon += 1
            else:
                mon = 1
                year += 1
            month = '%d-%02d' %(year, mon)
        return events

def scrape_events(begin, end):
    
    scraper = EventQuery(begin, end)
    events = scraper.getEvents()
    keys = events[0].keys()
    print '## %s'% encode('\t'.join(keys))
    for event in events:
        values = event.values()
        print encode('\t'.join(values))
        
        
    
    
    
if __name__ == '__main__':
    usage = "scs_events.py -b from_month -e to_month"
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('-b', '--begin',
                      help='begin month e.g., 2013-12',
                      required=True
                      )
    
    parser.add_argument('-e', '--end',
                      help='end month e.g., 2014-02',
                      required=True
                      )
    
    
    args = parser.parse_args()

    scrape_events(args.begin, args.end)
    
                    
